package kaldi;
// onlinebin/java-online-audio-client/src/KaldiASR.java

// Copyright 2013 Polish-Japanese Institute of Information Technology (author: Danijel Korzinek)

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Kaldi {

	private Socket socket;

	private OutputStream output_stream;
	private BufferedReader input_reader;
	private Thread streamSender;
	private int buffer_size;

	enum OutputFormat {
		WORDS, WORDS_ALIGNED
	}

	public Kaldi(String host, int port) throws UnknownHostException, IOException {
		socket = new Socket(host, port);
		output_stream = socket.getOutputStream();
		input_reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), Options.KALDI_ENCODING));
		buffer_size = 4;
	}
	
	public Kaldi(String host, int port, int bs) throws UnknownHostException, IOException {
		socket = new Socket(host, port);
		output_stream = socket.getOutputStream();
		input_reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), Options.KALDI_ENCODING));
		this.buffer_size = bs;
	}

	public void recognize(InputStream input_stream, OutputProcess output_process) throws IOException {

		streamSender = new Thread(new StreamSender(input_stream, output_stream, buffer_size));
		streamSender.start();

		int unchanged = 0;
		int last = -1;

		while (true) {
			
			String header = "";
			char[] chars = new char[8192];
			int i=0;
			do {
				i = input_reader.read(chars,0,8192);
				if (last == i) unchanged++;
				header += new String(chars,0,i);
				last = i;
			} while (!header.contains("\n"));
			
			String[] splitHeader = header.split("\\n");
			
			for (int h = 0; h < splitHeader.length; h++) {
				header = splitHeader[h];
				if (header.isEmpty() || "".equals(header)) continue;
				if (!header.startsWith("RESULT:")) {
					if (header.startsWith("PARTIAL:")) {
						header = header.replace("PARTIAL:", "");
						String word[] = header.split(",");
						if (output_process != null)
							output_process.addWord(new OutputProcess.Word(word[0], Float.parseFloat(word[1]), Float.parseFloat(word[2])));
						continue;
					}
				}
				
				if (header.startsWith("DONE:") || header.startsWith("RESULT:")) {
					if (output_process != null) {
						output_process.newSet();
						output_process.endOfUtt();
						return;
						
					}
					continue;
				}
			
			}

			String params[] = header.substring(7).split(",");

			int num = 0;
			OutputFormat format = OutputFormat.WORDS_ALIGNED;

			for (String param : params) {
				String ptok[] = param.split("=");

				if (ptok[0].equals("NUM"))
					num = Integer.parseInt(ptok[1]);
				else if (ptok[0].equals("FORMAT")) {
					if (ptok[1].equals("WSE"))
						format = OutputFormat.WORDS_ALIGNED;
					else if (ptok[1].equals("W"))
						format = OutputFormat.WORDS;
					else
						throw new RuntimeException("Output format " + ptok[1] + " is not supported");
				}
//else
//					Main.log("WARNING: unknown parameter in header: " + ptok[0]);
			}


			
			for (i = 0; i < num; i++) {
				String line = "";
				do {
					i = input_reader.read(chars,0,8192);
					line += new String(chars,0,i);
					
				} while (!line.contains("\n"));
				
				String[] splitLine = line.split("\\n");
				
				for (int h = 0; h < splitLine.length; h++) {
				    line = splitLine[h];
					if (format == OutputFormat.WORDS_ALIGNED) {
						String word[] = line.split(",");
						if (word.length > 1) {
							if (output_process != null)
								output_process.addWord(new OutputProcess.Word(word[0], Float.parseFloat(word[1]), Float.parseFloat(word[2])));
						}
					} else if (format == OutputFormat.WORDS) {
						if (output_process != null)
							output_process.addWord(new OutputProcess.Word(line, 0, 0));
					}
					
				}
			}

		}

	}

	public void close() throws IOException {

		try {
//			just in case the child thread is still expecting input, interrupt and wait for it
			if (streamSender != null) {
				streamSender.interrupt();
				streamSender.join();
			}
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		finally {
			output_stream.flush();
			output_stream.close();
			input_reader.close();
			socket.close();
		}
	}
}
